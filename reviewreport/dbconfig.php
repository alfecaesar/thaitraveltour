<?php

// https://medoo.in/doc

require  'Medoo.php';


use Medoo\Medoo;
 
$database = new Medoo([
	'database_type' => 'mysql',
	'database_name' => 'c2silver_wp250',
	'server' => 'localhost',
	'username' => 'c2silver_wp250',
	'password' => 'p62ShV]k]8'
]);


// create project table
$database->create("reports", [
	"id" => [
		"INT",
		"NOT NULL",
		"AUTO_INCREMENT",
		"PRIMARY KEY"
	],
	"date" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"roomnumber" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"roomtype" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"brand" => [
		"VARCHAR(64)",
		"NOT NULL"
	],
	"nationality" => [
		"VARCHAR(64)",
		"NOT NULL"
	],
	"reviewscore" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"staff" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"facilities" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"cleanliness" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"comfort" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"valueformoney" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"location" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"originalcomment" => [
		"TEXT(10000)",
		"NOT NULL"
	],
	"translation" => [
		"TEXT(10000)",
		"NOT NULL"
    ],

]);

// drop project table
// $database->drop("project");



?>