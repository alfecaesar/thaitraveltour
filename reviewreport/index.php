<?php include_once('config.php');?>
<!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:addthis="https://www.addthis.com/help/api-spec"
    prefix="og: http://ogp.me/ns#" class="no-js">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Review Report</title>

    <?php include 'include/assets.php'; ?>
</head>

<body>

    <?php include 'include/header.php'; ?>


    <div id="searcharea">
        <?php
	$condition	=	'';
	if(isset($_REQUEST['roomnumber']) and $_REQUEST['roomnumber']!=""){
		$condition	.=	' AND roomnumber LIKE "%'.$_REQUEST['roomnumber'].'%" ';
	}
	if(isset($_REQUEST['roomtype']) and $_REQUEST['roomtype']!=""){
		$condition	.=	' AND roomtype LIKE "%'.$_REQUEST['roomtype'].'%" ';
	}
	if(isset($_REQUEST['brand']) and $_REQUEST['brand']!=""){
		$condition	.=	' AND brand LIKE "%'.$_REQUEST['brand'].'%" ';
	}
	$reportData	=	$db->getAllRecords('reports','*',$condition,'ORDER BY id DESC');
	?>
        <div class="container">

            <div class="card">
                <div class="card-header"><i class="fa fa-fw fa-globe"></i> <strong>Browse Reviews</strong> <a
                        href="add-report.php" class="float-right btn btn-dark btn-sm"><i
                            class="fa fa-fw fa-plus-circle"></i> Add Review</a></div>
                <div class="card-body">
                    <?php
				if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rds"){
					echo	'<div class="alert alert-success alert-dismissible fade show" role="alert"><i class="fa fa-thumbs-up"></i> Record deleted successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
				}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rus"){
					echo	'<div class="alert alert-success alert-dismissible fade show" role="alert"><i class="fa fa-thumbs-up"></i> Record updated successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
				}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rnu"){
					echo	'<div class="alert alert-warning alert-dismissible fade show" role="alert"><i class="fa fa-exclamation-triangle"></i> You did not change any thing!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
				}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
					echo	'<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="fa fa-exclamation-triangle"></i> There is some thing wrong <strong>Please try again!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
				}
				?>
                    <div class="col-sm-12">
                        <h5 class="card-title"><i class="fa fa-fw fa-search"></i> Find Review</h5>
                        <form method="get">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Room Number</label>
                                        <input type="text" name="roomnumber" id="roomnumber" class="form-control"
                                            value="<?php echo isset($_REQUEST['roomnumber'])?$_REQUEST['roomnumber']:''?>"
                                            placeholder="Room Number">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Room Type</label>
                                        <input type="text" name="roomtype" id="roomtype" class="form-control"
                                            value="<?php echo isset($_REQUEST['roomtype'])?$_REQUEST['roomtype']:''?>"
                                            placeholder="Room Type">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Brand</label>
                                        <input type="text" name="brand" id="brand" class="form-control"
                                            value="<?php echo isset($_REQUEST['brand'])?$_REQUEST['brand']:''?>"
                                            placeholder="Brand">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <div>
                                            <button type="submit" name="submit" value="search" id="submit"
                                                class="btn btn-primary"><i class="fa fa-fw fa-search"></i>
                                                Search</button>
                                            <a href="<?php echo $_SERVER['PHP_SELF'];?>" class="btn btn-danger"><i
                                                    class="fa fa-fw fa-sync"></i> Clear</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="tablearea">
        <table class="table table-striped table-bordered">
            <thead>
                <tr class="bg-primary text-white">
                    <th width="2%">#</th>
                    <th width="5%">Date</th>
                    <th width="5%">Room Number</th>
                    <th width="5%">Room Type</th>
                    <th width="5%">Brand</th>
                    <th width="5%">Nationality</th>
                    <th width="5%">Review Score</th>
                    <th width="5%">Staff</th>
                    <th width="5%">Facilities</th>
                    <th width="5%">Cleanliness</th>
                    <th width="5%">Comfort</th>
                    <th width="5%">Location</th>
                    <th width="5%">Value for Money</th>
                    <th width="15%">Original Comment</th>
                    <th width="15%">Translation</th>
                    <th width="8%" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
					$s	=	'';
					foreach($reportData as $val){
						$s++;
					?>
                <tr>
                    <td><?php echo $val['id'];?></td>
                    <td><?php echo $val['date'];?></td>
                    <td><?php echo $val['roomnumber'];?></td>
                    <td><?php echo $val['roomtype'];?></td>
                    <td><?php echo $val['brand'];?></td>
                    <td><?php echo $val['nationality'];?></td>
                    <td><?php echo $val['reviewscore'];?></td>
                    <td><?php echo $val['staff'];?></td>
                    <td><?php echo $val['facilities'];?></td>
                    <td><?php echo $val['cleanliness'];?></td>
                    <td><?php echo $val['comfort'];?></td>
                    <td><?php echo $val['location'];?></td>
                    <td><?php echo $val['valueformoney'];?></td>
                    <td><?php echo $val['originalcomment'];?></td>
                    <td><?php echo $val['translation'];?></td>
                    <td align="center">
                    <!--
                        <a href="edit-report.php?editId=<?php echo $val['id'];?>" class="text-primary"><i
                                class="fa fa-fw fa-edit"></i> Edit</a> |
                        <a href="delete.php?delId=<?php echo $val['id'];?>" class="text-danger"
                            onClick="return confirm('Are you sure to delete this report?');"><i
                                class="fa fa-fw fa-trash"></i> Delete</a>
                    -->
                        <a data-toggle="modal" data-target="#passwordConfirmEdit" href="javascript:void(0)" class="text-primary">
                            <i class="fa fa-fw fa-edit"></i> Edit</a> |
                        <a data-toggle="modal" data-target="#passwordConfirmDelete" href="javascript:void(0)" class="text-danger">
                            <i class="fa fa-fw fa-trash"></i> Delete</a>

<div class="modal fade" id="passwordConfirmEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Confirm Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group  text-left">
                <label>Enter Password To Edit</label>
                <input type="password" name="cpassworde_<?php echo $val['id'];?>" id="cpassworde_<?php echo $val['id'];?>" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button onclick="validatePassword('edit-report.php?editId=<?php echo $val['id'];?>','cpassworde_<?php echo $val['id'];?>')" type="button" class="btn btn-primary">Proceed to Edit</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="passwordConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Confirm Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group text-left">
                <label>Enter Password To Delete</label>
                <input type="password" name="cpasswordd_<?php echo $val['id'];?>" id="cpasswordd_<?php echo $val['id'];?>" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button onclick="validatePassword('delete.php?delId=<?php echo $val['id'];?>','cpasswordd_<?php echo $val['id'];?>')" type="button" class="btn btn-danger">Proceed to Edit</button>
        </div>
        </div>
    </div>
</div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!--/.col-sm-12-->





    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
    </script>

    <script type="text/javascript">
        var gpassword = 'Password1234!'
        function validatePassword(a,b){
            if($('input#'+b).val() === gpassword){
                document.location.href = a;
            }
        }
    </script>

</body>

</html>