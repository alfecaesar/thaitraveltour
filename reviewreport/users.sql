--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `date` varchar(24) NOT NULL,
  `roomnumber` varchar(24) NOT NULL,
  `roomtype` varchar(24) NOT NULL,
  `brand` varchar(64) NOT NULL,
  `nationality` varchar(64) NOT NULL,
  `reviewscore` varchar(24) NOT NULL,
  `staff` varchar(24) NOT NULL,
  `facilities` varchar(24) NOT NULL,
  `cleanliness` varchar(24) NOT NULL,
  `comfort` varchar(24) NOT NULL,
  `valueformoney` varchar(24) NOT NULL,
  `location` varchar(24) NOT NULL,
  `originalcomment` text(8000) NOT NULL,
  `translation` text(8000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `date`, `roomnumber`, `roomtype`, `brand`, `nationality`, `reviewscore`, `staff`, `facilities`, `cleanliness`, `comfort`, `valueformoney`, `location`, `originalcomment`, `translation`) VALUES
(1, 'Feb 04, 2020', '111', 'SSB', 'Khaosan Art Hotel', 'Thailand', '8.5', '10', '10', '10', '10', '10', '10', 'Best Hotel Ever!', '');
