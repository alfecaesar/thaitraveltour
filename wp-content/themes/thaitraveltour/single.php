<?php
/**
 * Template Name: Default Template
 *
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>

<div id="contentarea">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-xs-12">
				<section class="non-elementor-section-boxed bblog elementor-section-boxed wbox elementor-section">
					<div class="elementor-container" style="margin: auto;">
						<div class="elementor-row main-content">
							<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'loop-templates/content', 'single' ); ?>

							<?php understrap_post_nav(); ?>

							<?php
								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
								?>

							<?php endwhile; // end of the loop. ?>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-3 col-xs-12">
				<section class="non-elementor-section-boxed bblog elementor-section-boxed wbox elementor-section">
					<div class="elementor-container" style="margin: auto;">
						<div class="elementor-row">
							<?php get_template_part( 'sidebar-templates/sidebar', 'right' ); ?>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

</div><!-- #contentarea -->

<?php get_footer();
