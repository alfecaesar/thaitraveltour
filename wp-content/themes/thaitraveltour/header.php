<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,400i,700&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	<div id="toparea">
		<div class="container">
			<div class="row">
				<div class="col-2 logo_c"><a href="/"><img src="http://www.alfe.thaitraveltour.com/wp-content/uploads/2020/02/art-travel-logo-Converted-profile.png" alt="Art Travel"></a></div>
				<div class="col-10 menu_c">
					<nav>
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => '',
								'container_id'    => 'MainMenuContainer',
								'menu_class'      => '',
								'fallback_cb'     => '',
								'menu_id'         => 'MainMenu',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						); ?>
					</nav>
					<div class="mobile-nav">
						<a id="mobile-nav-btn" href="#"><i class="fas fa-bars"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php if ( is_front_page() ) { ?>

	<div id="bannerarea">
		<?php
			echo do_shortcode('[smartslider3 slider=2]');
		?>
	</div>

	<?php get_template_part( 'global-templates/search', 'bar' ); ?>

	<?php }elseif ( is_page() || is_single() ) { ?>
	
	<?php if (has_post_thumbnail( $post->ID ) ){ ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<div id="bannerarea" class="inner" style="background-image: url('<?php echo $image[0]; ?>')"> 	
	<?php }else{ ?>
		<div id="bannerarea" class="inner" style="background-image: url('http://www.alfe.thaitraveltour.com/wp-content/uploads/2020/02/banner-packages.jpg');"> 
	<?php } ?>
		<div class="container">
			<div class="row h-100 align-items-center">
				<div class="col-12 text-center my-auto">
					<h1><?php
						if(!is_blog()){
							the_title();
						}
						else{
							echo 'Travel Blog';
						}
					?></h1>
				</div>
			</div>
		</div>
	</div>
	
	<div id="breadcrumbsarea">
		<div class="container">
			<div class="row">
				<div class="col-12 text-left">
					<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
						}
					?>
				</div>
			</div>
		</div>
	</div>

<?php } ?>