<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

	<div id="footerarea">
		<div class="container">
			<div class="row footermenu">
				<div class="col-12">
					<h3>Browse All Thailand Attractions by Destinations</h3>
					<nav>
						<ul>
						<?php
							$terms = get_terms(
								array(
									'taxonomy'   => 'destination',
									'hide_empty' => false,
								)
							);

							// Check if any term exists
							if ( ! empty( $terms ) && is_array( $terms ) ) {
								// Run a loop and print them all
								foreach ( $terms as $term ) { ?>
									<li>
										<a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
											<?php echo $term->name; ?>
										</a>
									</li><?php
								}
							} 

							?>
						</ul>
					</nav>
				</div>
				<div class="col-12">
					<hr />
				</div>
			</div>
			<div class="row footerbottom">
				<div class="col-md-6 col-sm-12 text-left ftb-col">
					Copyright © 2020 Art Travel. All rights reserved.
				</div>
				<div class="col-md-6 col-sm-12 text-right ftb-col"> 
					<?php
						if ( is_active_sidebar( 'footer-bottom-widget' ) ) { 
							dynamic_sidebar( 'footer-bottom-widget' ); 
						}
					?>
				</div>
			</div>
		</div>
	</div>

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

