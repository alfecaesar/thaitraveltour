<?php
/**
 * The template for displaying 404 pages (not found)
 *
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>


<div id="bannerarea" class="inner" style="background-image: url('http://www.alfe.thaitraveltour.com/wp-content/uploads/2020/02/banner-about-us.jpg')"> 	
		<div class="container">
			<div class="row h-100 align-items-center">
				<div class="col-12 text-center my-auto">
					<h1><?php _e( 'Page Not Found' ); ?></h1>
				</div>
			</div>
		</div>
	</div>

	<div id="breadcrumbsarea">
		<div class="container">
			<div class="row">
				<div class="col-12 text-left">
					
					<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
						}
					?>
				</div>
			</div>
		</div>
	</div>

<div id="contentarea"> 
		<div class="elementor">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
					<section class="elementor-section-boxed wbox elementor-section">
						<div class="elementor-container" style="margin: auto;">
							<div class="elementor-row">
								<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?' ); ?></p>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
</div><!-- #contentarea -->

<?php get_footer();






