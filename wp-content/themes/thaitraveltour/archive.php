<?php
/**
 * Template Name: Default Template
 *
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>


<?php          
    $obj = get_queried_object();
    $image_id = get_term_meta ( $obj->term_id, 'category-image-id', true );  
?>	


<div id="bannerarea" class="inner" style="background-image: url('<?php echo wp_get_attachment_image_url ( $image_id, 'full' );  ?>')"> 	
		<div class="container">
			<div class="row h-100 align-items-center">
				<div class="col-12 text-center my-auto">
					<h1><?php echo $obj->name ; ?></h1>
				</div>
			</div>
		</div>
	</div>

	<div id="breadcrumbsarea">
		<div class="container">
			<div class="row">
				<div class="col-12 text-left">
					
					<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
						}
					?>
				</div>
			</div>
		</div>
	</div>

<div id="contentarea"> 
		<div class="elementor">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
					<section class="mb-5 non-elementor-section-boxed elementor-section-boxed wbox elementor-section">
						<div class="elementor-container" style="margin: auto;">
							<div class="elementor-row">
                                <?php echo wpautop( wptexturize( $obj->description ) ); ?>
							</div>
						</div>
					</section>

					<section class="non-elementor-section-boxed elementor-section-boxed wbox elementor-section">
						<div class="elementor-container" style="margin: auto;">
							<div class="elementor-row">
								<h2>Other Destinations</h2>

                                <?php echo do_shortcode('[popularDestinations]'); ?>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
</div><!-- #contentarea -->

<?php get_footer();
