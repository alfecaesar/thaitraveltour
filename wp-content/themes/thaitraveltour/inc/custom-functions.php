<?php


// DELETE AUTO DRAFT ON DATABASE
// RUN IN SQL
// DELETE FROM wpqm_posts WHERE post_status = 'auto-draft' 

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


// FOOTER - SOCIAL MEDIA WIDGET
register_sidebar( array(
    'name' => __( 'Art Travel - Footer Follow Us' ),
    'id' => 'footer-bottom-widget',
    'description' => __( 'A widget area located to bottom area.' ),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
));


/* ADMIN STYLES */
add_action('admin_head', 'admin_styles');
function admin_styles() {
  echo '<style>
    table.wp-list-table td.description p, table.wp-list-table td.description ul{
        display: none;
    }
  </style>';
}


function is_blog () {
	global  $post;
	$posttype = get_post_type($post );
	return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
}



/* POPULAR DESTINATION LIST */
function displayPopularDestination(){
    if(is_front_page() || is_singular() || is_archive()) {


        $terms = get_terms(
            array(
                'taxonomy'   => 'destination',
                'hide_empty' => false,
                'number' => 4
            )
        );

        ?>
        <div class="row">
        <?php
            
        // Check if any term exists
        if ( ! empty( $terms ) && is_array( $terms ) ) {
            // Run a loop and print them all
            foreach ( $terms as $term ) { 
                ?>            
                    <div class="col-md-3 col-sm-6">
                        <div class="pbbox">
                                <div class="pbbox-img">
                                    <?php 
                                        $image_id = get_term_meta ( $term->term_id, 'category-image-id', true );
                                        echo wp_get_attachment_image ( $image_id );
                                    ?>
                                    
                                </div>
                                <div class="pbbox-action">
                                    <?php echo $term->name; ?>
                                    <a href="<?php echo esc_url( get_term_link( $term ) ) ?>"><i class="fas fa-angle-double-right"></i></a>
                                </div>
                            </div>
                    </div>
                <?php
            }
        } 
        ?>

        </div>

        <?php
    }
}
add_shortcode( 'popularDestinations', 'displayPopularDestination' );
// [popularDestinations]




/* RECOMMENDED PACKAGES LIST */
function displayrecommendedPackages(){
    if(is_front_page() || is_singular() || is_archive()) {
    $args = array(
        'post_type'   => 'packages',
        'post_status' => 'published',
        'posts_per_page' => 4,
    );
    $recommendedPackages = new WP_Query( $args );
    if($recommendedPackages->have_posts()){
        ?>
            <div class="row">
        <?php
                while( $recommendedPackages->have_posts() ) :
                    $recommendedPackages->the_post();

                    $thumbnail = get_field( 'thumbnail' );
                    $title = get_the_title();
                        ?>
                            <div class="col-md-3 col-sm-6">
                                <div class="pbbox">
                                        <div class="pbbox-img">
                                            <img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>" />
                                        </div>
                                        <div class="pbbox-action">
                                            <?php echo $title; ?>
                                            <a href="<?php echo get_permalink(); ?>"><i class="fas fa-angle-double-right"></i></a>
                                        </div>
                                </div>
                            </div>
                        <?php
                endwhile;
                wp_reset_postdata();
        ?>
            </div>
        <?php
    }
    }
}
add_shortcode( 'recommendedPackages', 'displayrecommendedPackages' );
// [recommendedPackages]




/* RECOMMENDED PACKAGES LIST */
function displaytravelBlog(){
    if(is_front_page() || is_singular() || is_archive()) {
        $args = array(
            'post_type'   => 'post',
            'post_status' => 'published',
            'posts_per_page' => 3,
        );
        $travelBlog = new WP_Query( $args );
        if($travelBlog->have_posts()){
            ?>
                <div class="row">
            <?php
                    while( $travelBlog->have_posts() ) :
                        $travelBlog->the_post();

                        $thumbnail = get_field( 'thumbnail' );
                        $title = get_the_title();
                            ?>
                                <div class="col-md-4 col-sm-6">
                                    <div class="pbbox blog">
                                            <div class="pbbox-img">
                                                <img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>" />
                                            </div>
                                            <div class="pbbox-title">
                                                <h3><?php echo $title; ?></h3>
                                            </div>
                                            <div class="pbbox-meta">
                                                29-Jan-2020 | <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php echo get_the_author(); ?></a>
                                            </div>
                                            <div class="pbbox-summary">
                                                <p><?php echo the_field( 'summary' ); ?></p>
                                            </div>
                                            <div class="pbbox-btn">
                                                <a href="<?php echo get_permalink(); ?>">Read More <i class="fas fa-angle-double-right"></i></a>
                                            </div>
                                    </div>
                                </div>
                            <?php
                    endwhile;
                    wp_reset_postdata();
            ?>
                </div>
            <?php
        }
    }
}
add_shortcode( 'travelBlog', 'displaytravelBlog' ); 
// [travelBlog]



/* Services LIST */
function displayservicesList(){
    if(is_page()) {
        $args = array(
            'post_type'   => 'services',
            'post_status' => 'published',
            'posts_per_page' => 3,
        );
        $servicesList = new WP_Query( $args );
        if($servicesList->have_posts()){
            ?>
                <div class="row">
            <?php
                    while( $servicesList->have_posts() ) :
                        $servicesList->the_post();

                        $thumbnail = get_field( 'image' );
                        $description = get_field( 'description' );
                        $title = get_the_title();
                            ?>
                                <div class="col-md-4 col-sm-12">
                                    <div class="pbbox services text-center">
                                            <div class="pbbox-img">
                                                <img src="<?php echo $thumbnail; ?>" alt="<?php echo $title; ?>" />
                                            </div>
                                            <div class="pbbox-title">
                                                <h3><?php echo $title; ?></h3>
                                            </div>
                                            <div class="pbbox-description">
                                                <p><?php echo $description; ?></p>
                                            </div>
                                    </div>
                                </div>
                            <?php
                    endwhile;
                    wp_reset_postdata();
            ?>
                </div>
            <?php
        }
    }
}
add_shortcode( 'servicesList', 'displayservicesList' ); 
// [servicesList]



/* Counter LIST */
function displaycounterList(){
    if(is_page()) {
        $args = array(
            'post_type'   => 'counter',
            'post_status' => 'published',
            'posts_per_page' => 3,
        );
        $counterList = new WP_Query( $args );
        if($counterList->have_posts()){
            ?>
                <div class="row">
            <?php
                    while( $counterList->have_posts() ) :
                        $counterList->the_post();

                        $value = get_field( 'value' );
                        $title = get_the_title();
                            ?>
                                <div class="col-md-4 col-sm-12">
                                    <div class="pbbox counter text-center">
                                            <div class="pbbox-bold-txt">
                                                <?php echo $value; ?>
                                            </div>
                                            <div class="pbbox-title">
                                                <h3><?php echo $title; ?></h3>
                                            </div>
                                    </div>
                                </div>
                            <?php
                    endwhile;
                    wp_reset_postdata();
            ?>
                </div>
            <?php
        }
    }
}
add_shortcode( 'counterList', 'displaycounterList' ); 
// [counterList]



/* Review LIST */
function displayreviewList(){
    if(is_page()) {
        $args = array(
            'post_type'   => 'reviews',
            'post_status' => 'published',
            'posts_per_page' => 999,
        );
        $reviewList = new WP_Query( $args );
        if($reviewList->have_posts()){
            ?>
                
            <?php
                    while( $reviewList->have_posts() ) :
                        $reviewList->the_post();

                        $image = get_field( 'image' );
                        $message = get_field( 'message' );
                        $full_name = get_field( 'full_name' );
                        $label = get_field( 'label' );
                        $rating = get_field( 'rating' );
                        $title = get_the_title();
                            ?>
                            <div class="row review-box">
                                <div class="col-md-3 col-xs-12">
                                    <div class="review-img">
                                        <img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" />
                                    </div>
                                </div>
                                <div class="col-md-9 col-xs-12">
                                    <div class="review-content">
                                        <div class="review-msg">
                                            <p><?php echo $message; ?></p>
                                        </div>
                                        <div class="review-author">
                                            <span class="ra_1"><?php echo $full_name; ?></span> | <span class="ra_2"><?php echo $label; ?></span>
                                        </div>
                                        <div class="review-rating">
                                            <?php
                                                for($i=1; $i<=5; $i++){
                                                    if($i<=$rating){
                                                        echo '<i class="fas fa-star"></i>';
                                                    }
                                                    else{
                                                        echo '<i class="far fa-star"></i>';
                                                    }
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="review-box-border"></div>
                                </div>
                            </div>
                            <?php
                    endwhile;
                    wp_reset_postdata();
            ?>
                
            <?php
        }
    }
}
add_shortcode( 'reviewList', 'displayreviewList' ); 
// [reviewList]



/*
function displayPopularDestination(){
    if(is_front_page() || is_singular() || is_archive()) {
    $args = array(
        'post_type'   => 'destinations',
        'post_status' => 'published',
        'posts_per_page' => 4,
    );
    $popularDestinations = new WP_Query( $args );
    if($popularDestinations->have_posts()){
        ?>
            <div class="row">
        <?php
                while( $popularDestinations->have_posts() ) :
                    $popularDestinations->the_post();

                    $thumbnail = get_field( 'thumbnail' );
                    $title = get_the_title();
                        ?>
                            <div class="col-md-3 col-sm-6">
                                <div class="pbbox">
                                        <div class="pbbox-img">
                                            <img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>" />
                                        </div>
                                        <div class="pbbox-action">
                                            <?php echo $title; ?>
                                            <a href="<?php echo get_permalink(); ?>"><i class="fas fa-angle-double-right"></i></a>
                                        </div>
                                </div>
                            </div>
                        <?php
                endwhile;
                wp_reset_postdata();
        ?>
            </div>
        <?php
    }
    }
}
add_shortcode( 'popularDestinations', 'displayPopularDestination' );
*/