<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/*
register_post_type( 'destinations',
    array(
        'labels' => array(
            'name' => __( 'Destinations', 'ct' ),
            'singular_name' => __( 'Destination', 'ct' ),
            'all_items' => __( 'All Destinations', 'ct' )
        ),
        'public' => true,
        'has_archive' => false,

        'menu_icon' => 'dashicons-palmtree',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions')
    )
);


register_post_type( 'packages',
    array(
        'labels' => array(
            'name' => __( 'Packages', 'ct' ),
            'singular_name' => __( 'Package', 'ct' ),
            'all_items' => __( 'All Packages', 'ct' )
        ),
        'public' => true,
        'has_archive' => false,

        'menu_icon' => 'dashicons-layout',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions')
    )
);
*/


register_post_type( 'services',
    array(
        'labels' => array(
            'name' => __( 'Services', 'ct' ),
            'singular_name' => __( 'Service', 'ct' ),
            'all_items' => __( 'All Services', 'ct' )
        ),
        'public' => false,
        'has_archive' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'rewrite' => false,
        'show_ui' => true,
        'menu_icon' => 'dashicons-image-filter',
        'supports' => array('title')
    )
);


register_post_type( 'counter',
    array(
        'labels' => array(
            'name' => __( 'Counter', 'ct' ),
            'singular_name' => __( 'Counter', 'ct' ),
            'all_items' => __( 'All Counter', 'ct' )
        ),
        'public' => false,
        'has_archive' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'rewrite' => false,
        'show_ui' => true,
        'menu_icon' => 'dashicons-image-filter',
        'supports' => array('title')
    )
);


register_post_type( 'reviews',
    array(
        'labels' => array(
            'name' => __( 'Reviews', 'ct' ),
            'singular_name' => __( 'Review', 'ct' ),
            'all_items' => __( 'All Reviews', 'ct' )
        ),
        'public' => false,
        'has_archive' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'rewrite' => false,
        'show_ui' => true,
        'menu_icon' => 'dashicons-image-filter',
        'supports' => array('title')
    )
);

