<?php
/**
 * Template Name: Default Template
 *
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>




<div id="contentarea"> 

    <?php the_content(); ?>					


</div><!-- #contentarea -->

<?php get_footer();
