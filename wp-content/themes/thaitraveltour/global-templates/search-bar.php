<div id="searchbararea">
    <div class="container">
        <form>
            <div class="row">
                <div class="col-md-4 col-sm-12 form-group">
                    <select class="form-control" name="select_package" required>
                        <option>Select Package</option>
                    </select>
                </div>
                <div class="col-md-4 col-sm-12 form-group">
                    <select class="form-control" name="select_destination" required>
                        <option value="">Select Destination</option>
                        <?php
                                $terms = get_terms(
                                    array(
                                        'taxonomy'   => 'destination',
                                        'hide_empty' => false
                                    )
                                );
                                // Check if any term exists
                                if ( ! empty( $terms ) && is_array( $terms ) ) {
                                    // Run a loop and print them all
                                    foreach ( $terms as $term ) { 
                                        ?>            
                                            <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                                        <?php
                                    }
                                } 
                        ?>
                    </select>
                </div>
                <div class="col-md-4 col-sm-12 form-group">
                    <input type="submit" value="SEARCH" class="btn btn-xl">
                </div>
            </div>
        </form>
    </div>
</div>