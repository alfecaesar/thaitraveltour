var simplemaps_countrymap_mapdata={
  main_settings: {
   //General settings
    width: "800", //'700' or 'responsive'
    background_color: "#FFFFFF",
    background_transparent: "yes",
    border_color: "#ffffff",
    
    //State defaults
    state_description: "State description",
    state_color: "#88A4BC",
    state_hover_color: "#3B729F",
    state_url: "",
    border_size: 1.5,
    all_states_inactive: "no",
    all_states_zoomable: "yes",
    
    //Location defaults
    location_description: "Location description",
    location_url: "",
    location_color: "#e1a34c",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_size: 25,
    location_type: "square",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",
    
    //Label defaults
    label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",
   
    //Zoom settings
    zoom: "yes",
    manual_zoom: "yes",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,
    
    //Popup settings
    popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",
    
    //Advanced settings
    div: "map",
    auto_load: "yes",
    url_new_tab: "no",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website",
    popups: "detect",
    state_image_url: "",
    state_image_position: "",
    location_image_url: ""
  },
  state_specific: {
    THA374: {
      description: " "
    },
    THA375: {
      description: " "
    },
    THA376: {
      description: " "
    },
    THA377: {
      description: " "
    },
    THA378: {
      description: " "
    },
    THA379: {
      description: " "
    },
    THA380: {
      description: " "
    },
    THA382: {
      description: " "
    },
    THA383: {
      description: " "
    },
    THA385: {
      description: " "
    },
    THA386: {
      description: " "
    },
    THA387: {
      description: " "
    },
    THA388: {
      description: " "
    },
    THA389: {
      description: " "
    },
    THA390: {
      description: " "
    },
    THA391: {
      description: " "
    },
    THA392: {
      description: " "
    },
    THA393: {
      description: " "
    },
    THA394: {
      description: " "
    },
    THA395: {
      description: " "
    },
    THA396: {
      description: " "
    },
    THA397: {
      description: " "
    },
    THA398: {
      description: " "
    },
    THA399: {
      description: " "
    },
    THA400: {
      description: " "
    },
    THA401: {
      description: " "
    },
    THA402: {
      description: " "
    },
    THA403: {
      description: " "
    },
    THA404: {
      description: " "
    },
    THA405: {
      description: " "
    },
    THA406: {
      description: " "
    },
    THA407: {
      description: " "
    },
    THA408: {
      description: " "
    },
    THA409: {
      description: " "
    },
    THA410: {
      description: " "
    },
    THA411: {
      description: " "
    },
    THA412: {
      description: " "
    },
    THA413: {
      description: " "
    },
    THA414: {
      description: " "
    },
    THA415: {
      description: " "
    },
    THA416: {
      description: " "
    },
    THA417: {
      description: " "
    },
    THA418: {
      description: " "
    },
    THA419: {
      description: " "
    },
    THA420: {
      description: " "
    },
    THA421: {
      description: " "
    },
    THA422: {
      description: " "
    },
    THA423: {
      description: " "
    },
    THA424: {
      description: " "
    },
    THA425: {
      description: " "
    },
    THA426: {
      description: " "
    },
    THA427: {
      description: " "
    },
    THA428: {
      description: " "
    },
    THA430: {
      description: " "
    },
    THA431: {
      description: " "
    },
    THA432: {
      description: " "
    },
    THA433: {
      description: " "
    },
    THA434: {
      description: " "
    },
    THA435: {
      description: " "
    },
    THA436: {
      description: " "
    },
    THA437: {
      description: " "
    },
    THA438: {
      description: " "
    },
    THA439: {
      description: " "
    },
    THA440: {
      description: " "
    },
    THA441: {
      description: " "
    },
    THA442: {
      description: " "
    },
    THA443: {
      description: " "
    },
    THA445: {
      description: " "
    },
    THA446: {
      description: " "
    },
    THA447: {
      description: " "
    },
    THA448: {
      description: " "
    },
    THA449: {
      description: " "
    },
    THA472: {
      description: " "
    },
    THA473: {
      description: " "
    },
    THA493: {
      description: " "
    },
    THA494: {
      description: " "
    },
    THA5499: {
      description: " "
    }
  },
  locations: {
    "0": {
      lat: "18.706064",
      lng: "98.981712",
      name: "Chiang Mai",
      description: "Chiang Mai province is in Northern Thailand. The Chiang Mai weather is always cooler than the rest of the country, with an average temperature of 25°C throughout the year. ",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/chiang-mai/"
    },
    "1": {
      lat: "19.911631",
      lng: "99.831810",
      name: "Chiang Rai",
      description: "Chiang Rai province is located in the northernmost of Thailand. It is a mountainous region with Rivers and jungles, so there is a lot of beautiful nature surrounding the area.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/chiang-rai/"
    },
    "2": {
      lat: "19.302031",
      lng: "97.965439",
      name: "Mae Hong Son",
      description: "Mae Hong Son province has another name as ‘the city of three mists’ because it is surrounded by high mountains with cold temperature and covered with mist all 3 seasons. ",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/mae-hong-son/"
    },
    "3": {
      lat: "17.005989",
      lng: "99.826691",
      name: "Sukhothai",
      description: "Sukhothai province was once the kingdom and the first capital of Thailand 700 years ago. The word ‘Sukhothai’ means ‘the dawn of happiness’. As the first capital of Thailand, it is one of the important and well-known cities among tourist for its long History.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/sukhothai/"
    },
    "4": {
      lat: "13.756331",
      lng: "100.501762",
      name: "Bangkok",
      description: "Bangkok is one of the world’s top tourist destinations. The city itself is a true tourist paradise, proved by the growing number of travelers coming each year.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/bangkok/"
    },
    "5": {
      lat: "14.348360",
      lng: "100.576271",
      name: "Ayutthaya",
      description: "The charm of ancient city of Ayutthaya Thailand continues to gain tourists’ attention as a historic attraction. Not only the old moments but also the new things that shine.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/ayutthaya/"
    },
    "6": {
      lat: "12.929120",
      lng: "100.921867",
      name: "Pattaya",
      description: "Pattaya is a popular place for both Thais and foreigners because the city has everything tourists need. Traveling there is easy as you can take your own car or ride a bus, a van, or a cab from Bangkok. There is also a ferry service from Hua Hin to Pattaya, which takes about an hour.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/pattaya/"
    },
    "7": {
      lat: "13.528650",
      lng: "99.813263",
      name: "Ratchaburi",
      description: "Ratchaburi is an ancient city of Dvaravati kingdom whose center was Nakhon Pathom. It is said that Nakhon Pathom was the center of spreading Buddhism. Ratchaburi city itself had been moved its city center for many years, until 1897 the city center was located at the present City Hall.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/ratchaburi/"
    },
    "8": {
      lat: "14.004000",
      lng: "99.549469",
      name: "Kanchanaburi",
      description: "Kanchanaburi is the third biggest province in Thailand. The area is mostly mountains and hilly terrains. Important Rivers are the Kwae Yai and the Kwae Noi which merge at the Mae Klong River.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/kanchanaburi/"
    },
    "9": {
      lat: "12.681720",
      lng: "101.256027",
      name: "Rayong",
      description: "Rayong is a province on the east coast of the Gulf of Thailand with 100-kilometer-long coast. The city is a center of Seafood and various kinds of fruits. With its richness of food and beautiful places, Rayong is one of must-see cities responding to various styles of visitors. ",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/rayong/"
    },
    "10": {
      lat: "12.242470",
      lng: "102.517693",
      name: "Trat",
      description: "Trat is Thailand’s eastern-most province, located about 315 kilometers from Bangkok. This small province borders Cambodia with the Khao Banthat mountain range forming a natural boundary between the two countries. ",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/trat/"
    },
    "11": {
      lat: "14.972380",
      lng: "102.099403",
      name: "Nakhon Ratchasima",
      description: "Nakhon Ratchasima or Korat Thailand is the most prosperous province of the East. Its richness of Culture is very interesting since it has its own folk song called ‘Pleng Korat.’ Nakhon Ratchasima is also important to Thai History. All ranges of tourist attractions are available here that you need to take quite some time to visit all of them.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/nakhon-ratchasima/"
    },
    "12": {
      lat: "15.229730",
      lng: "104.856117",
      name: "Ubon Ratchathani",
      description: "Ubon Ratchathani is a large province in the Northeast of Thailand, dating back to more than 200 years old. Ubon Ratchathani has many interesting and beautiful places to visit such as Pha Taem National Park, which is famous for its prehistoric rock paintings, Sam Pan Boak or the Grand Canyon of Thailand, Huai Sai Yai Waterfall, one of the most beautiful waterfalls of Northeastern Thailand, Kaeng Saphuee Public park, Wat Tham Khuha Sawan, and Wat Phrathat Nong Bua.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/ubon-ratchathani/"
    },
    "13": {
      lat: "17.414810",
      lng: "102.787079",
      name: "Udonthani",
      description: "Many biggest provinces in Thailand by size are in the north-eastern part of Thailand and Udon Thani is one of them. The province is like a logistic and tourism hub of the region and is important History-wise regarding the famous Ban Chiang source of prehistory civilization, one of the oldest civilization in the world.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/udonthani/"
    },
    "14": {
      lat: "17.487209",
      lng: "101.720291",
      name: "Loei",
      description: "Loei is a province in the northeast of Thailand. Its geographic features are like the North, that is, there are mountains with cool temperature and fog. One place that is popular among travellers who love challenging experience is Phu Kradung National Park. It is said that the couples can prove their love by going to the peak of Phu Kradung.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/loei/"
    },
    "15": {
      lat: "7.951933",
      lng: "98.338089",
      name: "Phuket",
      description: "What comes into the mind of travellers when we talk about Sea, sun and sand? Phuket must definitely be one of the answers. A number of exciting activities can be found on this Island. In the early days of regional maritime trade, the cape of Phuket was locally referred to as Jung Ceylon, while locals called it Thalang, which evolved to be the name of the main town to the north of the island. ",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/phuket/"
    },
    "16": {
      lat: "8.058260",
      lng: "98.916809",
      name: "Krabi",
      description: "Krabi, a province on southern Thailand’s Andaman coast, is an almost otherworldly region of labyrinthine archipelagos, where Islands seem to erupt vertically out of the Sea and secluded Beaches are only accessible by colorfully adorned long tail Boats. ",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/krabi/"
    },
    "17": {
      lat: "9.512017",
      lng: "100.013596",
      name: "Ko Samui",
      description: "Ko Samui is an Amphoe in Surat Thani province. It is located in the northeast of the city, 35 kilometers away from Don Sak Pier and 84 kilometers away from Surat Thani City, and is the third biggest Island in Thailand. ",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/ko-samui/"
    },
    "18": {
      lat: "9.743690",
      lng: "100.025590",
      name: "Ko Pha Ngan",
      description: "Pha Ngan Island is a world-famous one; especially Rin Beach where the well-known full moon party has been taken place in every full moon night. However, tourists visit there all year long, specifically in the summer.",
      size: "20",
      type: "circle",
      url: "http://www.alfe.thaitraveltour.com/destinations/ko-pha-ngan/"
    },
    "19": {
      lat: "9.131790",
      lng: "99.333618",
      name: "Suratthani",
      size: "20",
      type: "circle",
      description: "Surat Thani is a province in the lower southern Gulf of Thailand. There are many big and small Islands around the province. Koh Pha-Ngan is one of the popular islands in Surat Thani. It will be the favorite island of travellers who love swimming, sunbathing, listening to the sound of waves, going diving or snorkeling.",
      url: "http://www.alfe.thaitraveltour.com/destinations/suratthani/"
    }
  },
  labels: {},
  regions: {}
};